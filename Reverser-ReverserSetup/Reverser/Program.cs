﻿using System;
using Reverser.Resources;
using Library;

namespace Reverser
{
    class Program
    {
        static void Main(string[] args)
        {
            ReverseStringHelper reverserStringHelper = new ReverseStringHelper();
            Console.WriteLine(AppResources.WelcomeMessage);
            Console.WriteLine(AppResources.EnterRequest);
            var wordEntered = Console.ReadLine();
            Console.WriteLine(AppResources.Answer.Replace("{0}", reverserStringHelper.Reverse(wordEntered)));
            Console.WriteLine(AppResources.EndMessage);
            Console.Read();
        }
    }
}
