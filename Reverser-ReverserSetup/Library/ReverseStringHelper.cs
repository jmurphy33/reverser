﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    public class ReverseStringHelper
    {
        /// <summary>
        /// Reverses a given string
        /// </summary>
        /// <param name="normalString"></param>
        /// <returns></returns>
        public string Reverse(string normalString)
        {
            string reversedWord = "";
            var normalWordArray = normalString.ToArray();

            foreach (var item in normalString.ToCharArray())
            {
                reversedWord = item + reversedWord;
            }

            return reversedWord;
        }
    }
}
