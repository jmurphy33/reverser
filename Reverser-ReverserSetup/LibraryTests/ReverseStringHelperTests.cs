﻿using System;
using NUnit.Framework;
using Library;

namespace LibraryTests
{
    [TestFixture]
    public class ReverseStringHelperTests
    {
        private ReverseStringHelper _reverseStringHelper;

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void ReverString_ReversesAGivenString_ReturnsStringReversed()
        {
            // Arrange
            _reverseStringHelper = new ReverseStringHelper();
            string normalString = "tester 123";
            string reversedString = "321 retset";

            // Act
            var result =_reverseStringHelper.Reverse(normalString);

            // Assert
            Assert.AreEqual(reversedString, result);
        }
    }
}
